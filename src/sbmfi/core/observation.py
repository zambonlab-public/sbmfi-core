import numpy as np
import pandas as pd
import math
from typing import Iterable, Union, Dict, Tuple
from itertools import product, cycle
from collections import OrderedDict
import scipy.linalg
from cobra import Metabolite, DictList
from sbmfi.core.linalg import LinAlg
from sbmfi.core.model import LabellingModel
from sbmfi.core.metabolite import EMU
from sbmfi.core.polytopia import FluxCoordinateMapper, LabellingPolytope
from sbmfi.core.util import make_multidex, _strip_bigg_rex
from sbmfi.lcmsanalysis.formula import isotopologues
from sbmfi.lcmsanalysis.formula import Formula
from sbmfi.lcmsanalysis.adducts import emzed_adducts


def _get_bigg_metabolites(model_or_metabolites, strip_compartment=True):
    annot_metabolites = DictList()
    if hasattr(model_or_metabolites, 'metabolites'):
        iterator = DictList(model_or_metabolites.metabolites)
        if hasattr(model_or_metabolites, 'pseudo_metabolites'):
            iterator += DictList([met for met in model_or_metabolites.pseudo_metabolites if met not in iterator])
    elif hasattr(model_or_metabolites, '__iter__'):
        iterator = DictList(model_or_metabolites)
    else:
        raise ValueError('chgoser')
    for met in iterator:
        biggid = met.id
        if strip_compartment:
            biggid = _strip_bigg_rex.sub('', biggid)
        if biggid not in annot_metabolites:
            met = met.copy()
            met.id = biggid
            annot_metabolites.append(met)
    return annot_metabolites

def gen_annot_df(
        input,
        annotations='kegg.compound',
        first_annot=True,
        neutralize=True,
        strip_compartment=True,
    ):
    bigg_metabolites = _get_bigg_metabolites(input, strip_compartment=strip_compartment)

    data = {
        'met_id': bigg_metabolites.list_attr('id'),
        'name': bigg_metabolites.list_attr('name'),
        'formula': bigg_metabolites.list_attr('formula'),
        'charge': bigg_metabolites.list_attr('charge'),
    }
    if annotations is not None:
        annots = bigg_metabolites.list_attr('annotation')

    if annotations == 'all':
        data['annotations'] = annots
    elif isinstance(annotations, str):
        annotations = [annotations]
    elif annotations is None:
        annotations = []

    for annotation in annotations:
        tations = []
        for dct in annots:
            tation = dct.get(annotation, '')
            if first_annot and isinstance(tation, list):
                tation = tation[0]
            tations.append(tation)
        data[annotation] = tations

    annot_df = pd.DataFrame(data)
    # remove proteins, t-RNA and cofactor type things
    annot_df = annot_df.loc[~annot_df['formula'].str.contains(pat=r'[RX]\d*',regex=True)]
    annot_df['formula'] = annot_df.loc[:, ['formula', 'charge']].apply(
        lambda row: Formula(row['formula'], charge=row['charge']).to_chnops()
    , axis=1)
    annot_df.drop('charge', axis=1, inplace=True)

    if neutralize:
        def neutralproton(f_str):
            f = Formula(f_str)
            c = f['-']
            if (f['C'] > 0) and (c != 0):
                f += {'H': c, '-': -c}
            return f.to_chnops()
        annot_df['formula'] = annot_df['formula'].apply(neutralproton)

    annot_df['mz'] = annot_df['formula'].apply(lambda f: Formula(f).mass(ion=False))
    annot_df = annot_df.sort_values(by='mz').drop('mz', axis=1).reset_index(drop=True)
    return annot_df


def build_correction_matrix(
        # TODO incorporate a ppm argument so that we exclude correcting isotopes that are further than resolution away!
        formula, elements=None, isotope_threshold=1e-4, overall_threshold=0.0001, exclude_carbon=True, n_mdv=None
) -> np.array:
    formula = Formula(formula=formula).no_isotope()

    if exclude_carbon:
        n_C = formula.pop('C', 4) + 1 # here we exclude carbon
    else:
        n_C = formula.get('C', 4) + 1

    if n_mdv is None:
        n_mdv = n_C

    abundances = np.zeros(shape=n_mdv, dtype=np.double)
    for (formula, abundance) in isotopologues(
            formula=formula, elements_with_isotopes=elements, report_abundance=True,
            isotope_threshold=isotope_threshold, overall_threshold=overall_threshold, n_mdv=n_mdv
    ):
        shift = formula.shift()
        if shift < 0:
            raise ValueError(f'Shift under 0 {formula.to_chnops()}')
        abundances[shift] += abundance
    corr_mat = np.zeros((n_mdv, n_mdv), dtype=np.double)
    for i in range(n_mdv):
        np.fill_diagonal(corr_mat[i:], abundances[i])
    # corr_mat = corr_mat / corr_mat.sum(0)[None, :]
    return corr_mat

class MDV_ObservationModel(object):
    def __init__(
            self,
            model: LabellingModel,
            annotation_df: pd.DataFrame,
            correct_natab=False,
            clip_min=750.0,
            **kwargs,
    ):
        self._la = model._la
        self._annotation_df = annotation_df
        self._observation_df = self.generate_observation_df(model=model, annotation_df=annotation_df)
        self._n_o = self._observation_df.shape[0]
        self._natcorr = correct_natab
        if correct_natab:
            self._natab = self._set_natural_abundance_correction() # TODO this currently sucks
        self._state_id = model.state_id

        self._scaling = self._la.get_tensor(shape=(self._n_o, ))
        self._scaled = True
        self._mdv_scaling = self._la.get_tensor(shape=(len(model.state_id),))

        self._cmin = clip_min
        self._n_d = len(self.observation_id)

    @property
    def observation_id(self) -> pd.Index:
        return self._observation_df.index.copy()

    @property
    def state_id(self) -> pd.Index:
        return self._state_id.copy()

    @property
    def annotation_df(self):
        return self._annotation_df.copy()

    @property
    def observation_df(self):
        return self._observation_df.copy()

    @property
    def scaling(self):
        scaling = OrderedDict()
        for ion_id, indices in self._ionindices.items():
            scaling[ion_id] = self._la.tonp(self._scaling[indices][0])
        return pd.Series(scaling, name='scaling')

    @staticmethod
    def generate_observation_df(model: LabellingModel, annotation_df: pd.DataFrame, verbose=False):
        columns = pd.Index(['met_id', 'formula', 'adduct_name', 'nC13'])
        assert columns.isin(annotation_df.columns).all()
        annotation_df.reset_index(drop=True, inplace=True)  # necessary for annot_df_idx to be set correctly

        return_ids = model.state_id
        cols = []
        for i, (met_id, formula, adduct_name, nC13) in annotation_df.loc[:, columns].iterrows():
            if met_id not in model.measurements:
                if verbose:
                    print(f'{met_id} not in model.measurements')
                continue

            if adduct_name in ['M-H', 'M+H']:
                adduct_str = ''
            else:
                adduct_str = f'_{{{adduct_name}}}'
            oid = f'{met_id}{adduct_str}'  # id of the observation
            f = Formula(formula)
            met = model.measurements.get_by_id(met_id)
            if isinstance(met, EMU):
                n_C = len(met.positions)
            elif isinstance(met, Metabolite):
                n_C = met.elements['C']
            if (n_C != f['C']) and verbose:
                print(f'model measurement {met} with {n_C} carbons is different from annotated formula {formula}')
            model_return_id = f'{met_id}+{nC13}'
            state_idx = np.where(return_ids == model_return_id)[0][0]

            ion_row = emzed_adducts.loc[adduct_name]
            f = f * int(ion_row['m_multiplier']) \
                + Formula(ion_row['adduct_add']) \
                - Formula(ion_row['adduct_sub']) \
                + {'-': int(ion_row['z']) * -int(ion_row.get('sign_z', 1))}
            f = f.add_C13(nC13)
            isotope_decomposition = f.to_chnops()
            cols.append((i, met_id, oid, formula, adduct_name, nC13, isotope_decomposition, state_idx))

        obs_df = pd.DataFrame(cols, columns=[
            'annot_df_idx', 'met_id', 'ion_id', 'formula', 'adduct_name', 'nC13', 'isotope_decomposition', 'state_idx'
        ])
        obs_df.index = obs_df['ion_id'] + '+' + obs_df['nC13'].astype(str)
        obs_df.index.name = 'observation_id'
        obs_df = obs_df.drop_duplicates()  # TODO figure out a bug that replicates rows a bunch of times
        obs_df = obs_df.sort_values(by=['met_id', 'adduct_name', 'nC13'])  # sorting is essential for block-diagonal structure!

        if 'sigma' in annotation_df.columns:
            # sigma is defined per measurement
            obs_df['sigma'] = annotation_df.loc[obs_df['annot_df_idx'].values, 'sigma'].values

        def check_ion_equality(df, column_id, tol=1e-5):
            vals = df[column_id].values
            return (abs(vals - vals[0]) < tol).all()

        if 'omega' in annotation_df.columns:
            # omega is defined per ion
            obs_df['omega'] = annotation_df.loc[obs_df['annot_df_idx'], 'omega'].values
            obs_df['omega'] = obs_df['omega'].fillna(1.0)
            all_ion = obs_df.groupby('ion_id').apply(check_ion_equality, column_id='omega')
            if not all_ion.all():
                raise ValueError(
                    f'omega incorrectly set: {all_ion}, '
                    f'they should be equal across ions (metabolite + ionization)'
                )

        if 'total_I' in annotation_df.columns:
            # total_I is defined per ion
            obs_df['total_I'] = annotation_df.loc[obs_df['annot_df_idx'], 'total_I'].values
            all_ion = obs_df.groupby('ion_id').apply(check_ion_equality, column_id='total_I')
            if not all_ion.all():
                raise ValueError(
                    f'total_I incorrectly set: {all_ion}, '
                    f'they should be equal across ions (metabolite + ionization)'
                )
        return obs_df

    def _set_scaling(self, scaling: pd.Series, ionindices, transform_scaling=True):
        if scaling.index.duplicated().any():
            raise ValueError('double ions')
        # num_C = self._observation_df['isotope_decomposition'].apply(lambda x: Formula(x).no_isotope()['C'])
        # ions = num_C.index.str.rsplit('+', n=1, expand=True).to_frame().reset_index(drop=True)[0]
        odf = self._observation_df
        for ion_id, value in scaling.items():
            indices = ionindices.get(ion_id)
            if indices is not None:
                value = self._la.get_tensor(values=np.array([value]))
                self._scaling[indices] = value
                mdv_indices = self._la.get_tensor(values=odf.loc[(odf['ion_id'] == ion_id), 'state_idx'].values)
                self._mdv_scaling[mdv_indices] = value
        if (self._scaling <= 0.0).any():
            raise ValueError(f'a total intensity is not set: {self.scaling}')
        self._scaled = transform_scaling

    def check_x_meas(self, x_meas: pd.Series, atol=1e-3):
        # check whether the scaling makes sense and whether the clips are respected
        if isinstance(x_meas, pd.Series):
            x_meas = x_meas.to_frame().T
        if isinstance(x_meas, pd.DataFrame):
            x_meas = x_meas.values
        x_meas = self._la.atleast_2d(self._la.get_tensor(values=x_meas))
        if self._transformation is not None:
            x_meas = self._transformation.inv(x_meas)
        totals = (self._denom_sum @ x_meas.T)[self._denomi].T

        if self._scaled:
            # can unfortunately not check this for LCMS model, since we lose the total intensity information!
            correct_scaling = (abs(totals - self._scaling) <= atol).all()
            over_cmin = True if self._cmin is None else (totals >= self._cmin).all()

            if not all((correct_scaling, over_cmin)):
                raise ValueError(
                    f'the measurement cannot be produced by the observation model. '
                    f'Correct scaling: {correct_scaling}, '
                    f'over clip_min: {over_cmin}'
                )

    def _set_natural_abundance_correction(self, isotope_threshold=1e-4, correction_threshold=0.001):
        if self._observation_df.empty:
            raise ValueError('first set observations G')
        indices = []
        values = []
        tot_obs = 0

        for (Mid, isotope_decomposition), df in self._observation_df.groupby(by=['met_id', 'isotope_decomposition']):
            formula = Formula(formula=isotope_decomposition)
            mat = build_correction_matrix(
                formula=formula, isotope_threshold=isotope_threshold, overall_threshold=correction_threshold
            )
            slicer = df['nC13'].values
            normalizing_cons = mat[:, 0].sum()  # NOTE: making sure that the last row/ first col sum to 1
            mat /= normalizing_cons
            mat = mat[slicer, :][:, slicer]
            index = np.nonzero(mat)
            vals = mat[index]
            values.append(vals)
            indices.append(np.array(index, dtype=np.int64).T + tot_obs)
            tot_obs += df.shape[0]
        values = np.concatenate(values)
        indices = np.concatenate(indices)

        return self._la.get_tensor(shape=(tot_obs, tot_obs), values=values, indices=indices)

    def sample_observations(self, mdv, n_obs=3, **kwargs):
        raise NotImplementedError

    def __call__(self, mdv, n_obs=3, pandalize=False, **kwargs):
        index = None
        if isinstance(mdv, pd.DataFrame):
            index = mdv.index
            mdv = self._la.get_tensor(values=mdv.loc[:, self.state_id].values)

        result = self.sample_observations(mdv, n_obs=n_obs, **kwargs)

        if pandalize:
            n_samples = mdv.shape[0]
            if index is None:
                index = pd.RangeIndex(n_samples)
            n_obshape = max(1, n_obs)
            obs_index = pd.RangeIndex(n_obshape)
            index = make_multidex({i: obs_index for i in index}, 'samples_id', 'obs_i')
            if len(result.shape) > 2:
                result = self._la.tonp(result).transpose(1, 0, 2)
            result = result.reshape((n_samples*n_obshape, len(self.observation_id)))
            result = pd.DataFrame(result, index=index, columns=self.observation_id)
        return result


class _BlockDiagGaussian(object):
    """convenience class to set a bunch of indices and compute observation"""
    def __init__(self, linalg: LinAlg, observation_df: pd.DataFrame):
        self._la = linalg
        self._observation_df = observation_df
        self._no = observation_df.shape[0]

        sigma_indices = []
        tot_features = 0

        self._ionindices = {}  # for setting total intensity
        for denomi, ((model_id, ion_id, ion), df) in enumerate(
                self._observation_df.groupby(['met_id', 'ion_id', 'adduct_name'], sort=False)
        ):
            n_idion = df.shape[0]
            indices_feature = np.arange(tot_features, n_idion + tot_features, dtype=np.int64)
            self._ionindices[ion_id] = self._la.get_tensor(values=indices_feature)
            indices_block = list(product(indices_feature, indices_feature, [denomi]))
            sigma_indices += indices_block
            tot_features += n_idion

        _indices_columns = ['Σ_row_idx', 'Σ_col_idx', 'denomi', 'mdv_idx']
        sigma_indices = np.array(sigma_indices)[:, [1, 0, 2]]
        map_feat_to_mdv = dict(zip(range(self._observation_df.shape[0]), self._observation_df['state_idx'].values))
        sigma_indices = np.concatenate(
            [sigma_indices, np.vectorize(map_feat_to_mdv.get)(sigma_indices[:, 0])[:, None]], axis=1
        ).astype(np.int64)

        # these indices are used to distribute values into a sparse block-diagonal matrix
        self._indices = self._la.get_tensor(values=sigma_indices)
        self._row = self._la.get_tensor(values=sigma_indices[:, 0])
        self._col = self._la.get_tensor(values=sigma_indices[:, 1])
        self._denom = self._la.get_tensor(values=sigma_indices[:, 2])
        self._mdv = self._la.get_tensor(values=sigma_indices[:, 3])

        # these are booleans to indicate diagonal and upper triangular indices from self._indices above
        offdiag_uptri = np.array([True if (j > denomi) else False for (denomi, j, k, l) in sigma_indices])
        diagionals = sigma_indices[:, 0] == sigma_indices[:, 1]
        self._diag = self._la.get_tensor(values=diagionals)
        self._uptri = self._la.get_tensor(values=offdiag_uptri)

        # these are used to distribute values into a vector of features
        self._numi = self._la.get_tensor(values=observation_df['state_idx'].values)
        self._denomi = self._denom[self._diag]

        denom_sum_indices = np.unique(sigma_indices[:, [2, 1]], axis=0)
        self._denom_sum = self._la.get_tensor(
            shape=(sigma_indices[:, 2].max() + 1, self._observation_df.shape[0]),
            indices=denom_sum_indices, values=np.ones(denom_sum_indices.shape[0], dtype=np.double)
        )  # needs to be distributed with either self._denomi or self._denom!

        self._sigma = self._la.get_tensor(shape=(self._la._batch_size, self._no, self._no))
        self._sigma_1 = self._la.get_tensor(shape=(self._la._batch_size, self._no, self._no))
        self._chol = None
        self._bias = self._la.get_tensor(shape=(self._la._batch_size, self._no,))

    @property
    def sigma_1(self):
        if self._sigma_1 is None:
            self._sigma_1 = self._la.pinv(self._sigma, rcond= 1e-12, hermitian=False)
        return self._sigma_1

    @staticmethod
    def construct_sigma_x(observation_df: pd.DataFrame, diagonal: pd.Series = 0.01, corr=0.0):
        la = LinAlg(backend='numpy')
        if isinstance(diagonal, float):
            diagonal = pd.Series(diagonal, index=observation_df.index)
        elif isinstance(diagonal, pd.Series) and (len(diagonal) != observation_df.shape[0]):
            raise ValueError('wrong shape')

        diagonal = diagonal.loc[observation_df.index]
        idx = _BlockDiagGaussian(linalg=la, observation_df=observation_df)
        nf = len(diagonal)
        sigma = np.zeros((nf, nf))
        diagi = np.diag_indices(n=nf)
        variance = diagonal.values
        std = np.sqrt(variance)
        sigma[diagi] = variance / 2
        if corr > 0.0:
            sigma[idx._indices[idx._uptri, 0], idx._indices[idx._uptri, 1]] = \
                np.prod(std[idx._indices[idx._uptri, :2]], axis=1) * corr
        sigma += sigma.T
        return pd.DataFrame(sigma, index=observation_df.index, columns=observation_df.index)

    def set_sigma(self, sigma, verify=True):
        # this is mainly to set constant sigma

        if isinstance(sigma, pd.Series):
            sigma = self.construct_sigma_x(self._observation_df, diagonal=sigma)

        if isinstance(sigma, pd.DataFrame):
            sigma = self._la.get_tensor(
                values=sigma.loc[:, self._observation_df.index].loc[self._observation_df.index, :].values[None, :, :],
                squeeze=False
            )  # this throws error if wrong shape or features not represented

        if verify:
            sigma = self._la.get_tensor(values=sigma)  # making sure we have the correct type
            if not sigma.shape[-1] == sigma.shape[-2] == self._no:
                raise ValueError
            for i in range(sigma.shape[0]):

                variance = sigma[i, self._row[self._diag], self._col[self._diag]]
                std = self._la.sqrt(variance)
                offtri_cov = sigma[i, self._row[self._uptri], self._col[self._uptri]]
                corr_1 = std[self._row[self._uptri]] * std[self._col[self._uptri]]

                positive_var = all(variance >= 0.0)  # variance must be positive
                valid_cov = all(abs(offtri_cov) < corr_1)  # abs(correlation) <= 1
                is_diagonal = self._la.allclose(sigma, self._la.transax(sigma), rtol=1e-10)  # sigma must be diagonal
                if not (positive_var and valid_cov and is_diagonal):
                    raise ValueError

        self._sigma = sigma
        self._chol = self._la.cholesky(self._sigma)  # NB fails if not invertible!
        self._sigma_1 = None

    def compute_observations(self, s: pd.DataFrame, select=True, pandalize=False):
        # can take both simulations (full MDVs) or observation that need to be renormalized
        # s.shape = (n_simulations, n_mdv | n_observation)
        # select = True is used when passing MDVs, select = False is used when passing intensities
        index = None
        if isinstance(s, pd.DataFrame):
            index = s.index
            s = s.values
        s = self._la.get_tensor(values=s)
        observations_num = s
        if select:
            observations_num = s[..., self._numi]
        observations_denom = self._la.tensormul_T(self._denom_sum, observations_num)
        observations_denom[observations_denom == 0.0] = 1.0
        observations = observations_num / observations_denom[..., self._denomi]
        if pandalize:
            observations = pd.DataFrame(self._la.tonp(observations), index=index, columns=self._observation_df.index)
        return observations

    def sample_sigma(self, shape=(1, )):
        noise = self._la.randn((*shape, self._no,1))
        res =  (self._la.unsqueeze(self._chol, 1) @ noise).squeeze(-1)
        return res


class ClassicalObservationModel(MDV_ObservationModel, _BlockDiagGaussian):
    # TODO incorporate natural abundance!

    def __init__(
            self,
            model: LabellingModel,
            annotation_df: pd.DataFrame,
            sigma_df: pd.DataFrame = None,
            omega: pd.Series = None,
            correct_natab=False,
            clip_min=0.0,
            normalize=True,
            **kwargs,
    ):
        MDV_ObservationModel.__init__(
            self, model, annotation_df, correct_natab, clip_min, **kwargs
        )
        _BlockDiagGaussian.__init__(self, linalg=self._la, observation_df=self._observation_df)
        # TODO introduce scaling factor w as in Wiechert publications
        self._normalize = normalize

        # variables needed for dealing with a singular FIM
        self._permutations = {}
        self._selector = None

        if sigma_df is not None:
            self.set_sigma(sigma_df, verify=True)

        # TODO complete omega (scaling factors) check whether in _x_meas the things sum to
        if omega is None:
            ion_ids = self._observation_df['ion_id'].unique()
            omega = pd.Series(1.0, index=ion_ids)
        omega = omega.fillna(1.0)
        self._set_scaling(omega, self._ionindices)

    @staticmethod
    def build_models(
            model,
            annotation_dfs: Dict[str, Tuple[pd.DataFrame, pd.DataFrame, pd.Series]],
            normalize=True,
            clip_min=0.0,
    ) -> OrderedDict:
        obsims = OrderedDict()
        for labelling_id, (annotation_df, sigma_df, omega) in annotation_dfs.items():
            obsim = None
            if annotation_df is not None:
                obsim = ClassicalObservationModel(
                    model,
                    annotation_df=annotation_df,
                    sigma_df=sigma_df,
                    omega=omega,
                    clip_min=clip_min,
                    normalize=normalize,
                )
                if sigma_df is None:
                    sigma = _BlockDiagGaussian.construct_sigma_x(obsim.observation_df)
                    obsim.set_sigma(sigma, verify=True)
            obsims[labelling_id] = obsim
        return obsims

    def set_sigma_x(self, sigma_x: pd.DataFrame):
        self.set_sigma(sigma=sigma_x, verify=True)

    def sample_observations(self, mdv, n_obs=3, **kwargs):
        # clip_min will restrict out to the domain [0, ∞)
        # normalize will make sure that the Σ out = 1
        if self._chol is None:
            raise ValueError('set sigma_x')
        mdv = self._la.atleast_2d(mdv)  # shape = batch x n_mdv
        observations = self.compute_observations(s=mdv, select=True)  # batch x n_observables

        if self._natcorr:
            observations = self._natab @ observations

        observations *= self._scaling
        if n_obs == 0:  # this means we return the 'mean'
            return observations

        noise = self.sample_sigma(shape=(mdv.shape[0], n_obs))
        noisy_observations = observations[:, None, ...] + noise

        if (self._cmin is not None):
            noisy_observations = self._la.clip(noisy_observations, self._cmin, None)

        if self._normalize:
            if (self._cmin is None) or (self._cmin < 0.0):
                raise ValueError('cannot normalize if we have negative values')
            noisy_observations = self.compute_observations(noisy_observations, select=False)  # n_obs x batch x features
        return noisy_observations


class BoundaryObservationModel(object):
    def __init__(
            self,
            fcm: FluxCoordinateMapper,
            measured_boundary_fluxes: Iterable,
            biomass_id: str = None,
    ):
        self._call_kwargs = {}
        self._fcm = fcm
        self._la = fcm._la
        boundary_rxns = (self._fcm._Fn.S >= 0.0).all(0) | (self._fcm._Fn.S <= 0.0).all(0)
        self._bound_id = pd.Index(measured_boundary_fluxes)

        boundary_ids = self._fcm._Fn.S.columns[boundary_rxns]
        if biomass_id is not None:
            if not (biomass_id in self._fcm.fluxes_id):
                raise ValueError
            boundary_ids = boundary_ids.union([biomass_id])
        if not self._bound_id.isin(boundary_ids).all():
            raise ValueError('can only handle boundary fluxes and biomass for this observation model')

    @property
    def boundary_id(self):
        return self._bound_id.copy()

    def sample_observation(self, mu_bo, n_obs=1, **kwargs):
        raise NotImplementedError

    def log_lik(self, bo_meas, mu_bo):
        raise NotImplementedError

    def __call__(self, mu_bo, n_obs=1):
        # vape = boundary_fluxes.shape
        # flat = boundary_fluxes.view(vape[:-1].numel(), vape[-1])
        return self.sample_observation(mu_bo, n_obs, **self._call_kwargs)


class MVN_BoundaryObservationModel(BoundaryObservationModel):
    def __init__(
            self,
            fcm: FluxCoordinateMapper,
            measured_boundary_fluxes: Iterable,
            biomass_id: str = None,  # 'bm', 'BIOMASS_Ecoli_core_w_GAM'
            sigma_o=None,
            biomass_var=0.01,
            boundary_var=0.2,
    ):
        super(MVN_BoundaryObservationModel, self).__init__(
            fcm, measured_boundary_fluxes, biomass_id,
        )
        n = len(self._bound_id)
        if sigma_o is None:
            sigma_o = np.eye(n) * boundary_var
            if biomass_id is not None:
                bm_idx = self._bound_id.get_loc(biomass_id)
                sigma_o[bm_idx, bm_idx] = biomass_var
        self._sigma_o = self._la.get_tensor(values=sigma_o)
        self._sigma_o_1 = self._la.pinv(self._sigma_o, rcond= 1e-12, hermitian=False)

    def sample_observation(self, mu_bo, n_obs=1, **kwargs):
        if n_obs == 0:  # consistent with the MDV observation models!
            return mu_bo
        n, n_b = mu_bo.shape
        mu_bo = mu_bo[:, None, :]
        noise = self._la.randn(shape=(n, n_obs, len(self._bound_id))) @ self._sigma_o
        return abs(mu_bo + noise)  # .squeeze(0)

    def log_lik(self, bo_meas, mu_bo):
        mu_bo = self._la.atleast_2d(mu_bo)  # shape = batch x n_bo
        bo_meas = self._la.atleast_2d(bo_meas)  # shape = n_obs x n_bo
        diff = mu_bo[:, None, :] - bo_meas[:, None, :]  # shape = batch x n_obs x n_bo
        return - 0.5 * ((diff @ self._sigma_o_1) * diff).sum(-1)


if __name__ == "__main__":
    pass