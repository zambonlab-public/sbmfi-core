import numpy as np
import pandas as pd
import multiprocessing as mp  # TODO maybe dynamically change this import to torch.multiprocessing based on linalg?
from collections import OrderedDict
from typing import Iterable, Union, Dict, Tuple
from sbmfi.core.model import LabellingModel, EMU_Model
from sbmfi.core.observation import MDV_ObservationModel


def init_simulator(model: LabellingModel, epsilon=1e-12):
    global _MODEL, _EPSILON
    _MODEL = model
    model.build_simulator(free_reaction_id=model.fluxes_id, verbose=False)  # necessary after pickling
    _EPSILON = epsilon


def init_observer(
        model: LabellingModel,
        mdv_observation_models: Dict[str, MDV_ObservationModel],
        epsilon=1e-12
):
    init_simulator(model=model, epsilon=epsilon)
    global _OBSMODS
    _OBSMODS = mdv_observation_models


def simulator_worker(task: OrderedDict, model=None) -> OrderedDict:
    start_stop, input_labelling, fluxes_chunk, type_jacobian = task.values()

    if model is not None:
        MODEL = model
    else:
        MODEL = _MODEL

    n_state = len(MODEL.state_id)
    la = MODEL._la

    if MODEL.labelling_id != input_labelling.name:
        MODEL.set_input_labelling(input_labelling=input_labelling)

    mdv_chunk = la.get_tensor(  # by making this a tensor with -np.inf values, we can filter failed simulations
        values=np.full(shape=(fluxes_chunk.shape[0], n_state), fill_value=-np.inf, dtype=np.double)
    )

    if type_jacobian is not None:
        raise NotImplementedError

    step = la._batch_size
    stop = max(fluxes_chunk.shape[0], step)

    for i in range(0, stop, step, ):
        j = i + step
        if j > stop:
            j = stop
            i = j - step
        fluxes_batch = fluxes_chunk[i: j]
        try:
            MODEL.set_fluxes(fluxes=fluxes_batch, trim=False)  # trim has to be False!
            mdv_chunk[i: j] = MODEL.cascade()
        except Exception as e:
            print(1, e)

    # NB filter failed simulations (metabolite not summing to 1 or values outside of [0, 1]
    sum_1 = la.isclose(
        MODEL._sum @ mdv_chunk.T,
        la.ones(fluxes_chunk.shape[0], dtype=mdv_chunk.dtype), atol=_EPSILON).T.all(1)
    bounds = (mdv_chunk < 1.0 + _EPSILON).all(1) & (mdv_chunk > 0.0 - _EPSILON).all(1)
    # valid_idx = la.where(sum_1 & bounds)[0]
    validx_chunk = sum_1 & bounds

    result = OrderedDict([
        ('start_stop', start_stop),
        ('input_labelling', input_labelling),
        ('mdv_chunk', mdv_chunk),
        ('validx_chunk', validx_chunk),
    ])
    return result


def obervervator_worker(task: OrderedDict, model=None):
    simulator_task = OrderedDict(
        (k, task[k]) for k in ['start_stop', 'input_labelling', 'fluxes_chunk', 'type_jacobian']
    )
    result_chunk = simulator_worker(simulator_task, model=model)
    what = task['what']
    if what == 'mdv':
        return result_chunk

    if model is not None:
        MODEL = model
    else:
        MODEL = _MODEL

    mdv_chunk = result_chunk['mdv_chunk']

    n_obs = task['n_obs']
    observation_model = _OBSMODS[task['input_labelling'].name]
    n_obshape = max(1, n_obs)
    slicer = 0 if n_obs == 0 else slice(None)
    data_chunk = MODEL._la.get_tensor(shape=(mdv_chunk.shape[0], n_obshape, observation_model._n_d))
    bs = MODEL._la._batch_size

    for i in range(0, mdv_chunk.shape[0], bs):
        data_slice = observation_model(mdv=mdv_chunk[i: i + bs, :], n_obs=n_obs)  # TODO sizing!
        data_chunk[i: i + bs, slicer, :] = data_slice

    result_chunk['data_chunk'] = data_chunk

    if what != 'all':
        result_chunk.pop('mdv_chunk')

    return result_chunk


def simulator_tasks(
        fluxes,
        substrate_df: pd.DataFrame,
        fluxes_per_task: int,
        type_jacobian = None,
) -> OrderedDict:
    for labelling_id, row in substrate_df.iterrows():
        input_labelling = row[row > 0.0]
        for i in range(0, fluxes.shape[0], fluxes_per_task):
            fluxes_chunk = fluxes[i: i + fluxes_per_task]
            yield OrderedDict([
                ('start_stop', (i, i + fluxes_chunk.shape[0])),
                ('input_labelling', input_labelling),
                ('fluxes_chunk', fluxes_chunk),
                ('type_jacobian', type_jacobian),
            ])


def observator_tasks(
        fluxes,
        substrate_df: pd.DataFrame,
        fluxes_per_task: int,
        n_obs = 3,
        what = 'all',
):
    for task in simulator_tasks(fluxes, substrate_df, fluxes_per_task, type_jacobian = None):
        task['n_obs'] = n_obs
        task['what'] = what
        yield task


if __name__ == "__main__":
    pass