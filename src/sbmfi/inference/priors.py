import pandas as pd
import torch
import math
from torch.distributions.constraints import Constraint, _Dependent, _Interval
from sbmfi.core.model import LabellingModel
from sbmfi.core.linalg import LinAlg
from sbmfi.core.polytopia import FluxCoordinateMapper, sample_polytope
from typing import Iterable, Union
from torch.distributions import constraints
from torch.distributions import Distribution

class _CannonicalPolytopeSupport(_Dependent):  #
    _VTOL = 1e-13
    def __init__(
            self,
            fcm: FluxCoordinateMapper,
            validation_tol = _VTOL,
    ):
        polytope = fcm.make_theta_polytope()
        if polytope.S is not None:
            raise ValueError('only for cannonical polytopes, Av <= b')

        self._constraint_id = polytope.A.columns
        self._A = torch.from_numpy(polytope.A.values)
        self._b = torch.atleast_2d(torch.from_numpy(polytope.b.values + validation_tol)).T
        super().__init__(is_discrete=False, event_dim=self._A.shape[1])

    def to(self, *args, **kwargs):
        self._A = self._A.to(*args, **kwargs)
        self._b = self._b.to(*args, **kwargs)

    @property
    def constraint_id(self) -> pd.Index:
        return self._constraint_id.copy()

    def check(self, value: torch.Tensor) -> torch.Tensor:
        if value.dtype != self._A.dtype:
            value = value.to(self._A.dtype)
        vape = value.shape
        if len(vape) > 2:
            value = value.view((math.prod(vape[:-1]), vape[-1]))
        value = value[:, :self._A.shape[1]]
        valid = (self._A @ value.T <= self._b).T
        return valid.view(*vape[:-1], self._A.shape[0])


class _BasePrior(Distribution):
    def __init__(
            self,
            model: Union[FluxCoordinateMapper, LabellingModel],
            cache_size: int = 20000,
    ):
        # prior sampling variables
        self._ic = cache_size  # current index in the cache
        if isinstance(model, LabellingModel):
            if not model._is_built:
                raise ValueError('First build the model; choose kernel basis and basis coordinate system!')
            if model._la.backend != 'torch':
                linalg = LinAlg('torch', seed=model._la._backwargs['seed'])
                model = FluxCoordinateMapper(
                    model,
                    linalg=linalg,
                    free_reaction_id=model.labelling_reactions.list_attr('id'),
                    **{k: v for k, v in model._fcm.fcm_kwargs.items() if k != 'verbose'}
                )
            else:
                model = model._fcm

        self._fcm = model
        self._la = model._la

        self._cache_fill_kwargs = {'n': cache_size}
        self._theta_cache = torch.zeros((cache_size, self.n_theta), dtype=torch.double)  # cache to store dependent variables

        # passing validate_args={} will trigger support checking
        Distribution.__init__(self, event_shape=torch.Size((self.n_theta,)), validate_args={})

    @property
    def n_theta(self):
        # number of theta elements, depends on coordinate system for fluxes or number of ratios
        raise NotImplementedError

    @property
    def theta_id(self):
        raise NotImplementedError

    def to(self, *args, **kwargs):
        # this is useful for when we would like to sample on GPU
        raise NotImplementedError

    def _fill_caches(self, n=20000, **kwargs):
        # this function fills the cache with dependent variables in self._cache and fluxes in self._flux_cache
        raise NotImplementedError

    def rsample(self, sample_shape=torch.Size([])):
        # NB this always returns free fluxes in the thermodynamic coordinate system
        if not isinstance(sample_shape, torch.Size):
            sample_shape = torch.Size(sample_shape)

        n = sample_shape.numel()
        if n > self._theta_cache.shape[0]:
            self._cache_fill_kwargs['n'] = n

        jc = self._ic + n
        if jc > self._theta_cache.shape[0]:
            self._fill_caches(**self._cache_fill_kwargs)
            self._ic = 0
            jc = n
        sample = self._theta_cache[self._ic: jc].view(self._extended_shape(sample_shape))
        self._ic = jc
        return sample

    def sample_pandalize(self, n):
        result = self.sample((n,))
        return pd.DataFrame(self._la.tonp(result), columns=self.theta_id)


class _NetFluxPrior(_BasePrior):
    def __init__(
            self,
            model: Union[FluxCoordinateMapper, LabellingModel],
            cache_size: int = 20000,
    ):
        self._basis_points = None
        super(_NetFluxPrior, self).__init__(model, cache_size)

    # NB event_dim=1 means that the right-most dimension defines an event!
    @constraints.dependent_property(is_discrete=False, event_dim=1)
    def support(self):
        supp = _CannonicalPolytopeSupport(fcm=self._fcm)
        supp.to(dtype=torch.float32)  # TODO maybe pass dtype as a kwarg or maybe always enforce float32
        return supp

    @property
    def theta_id(self) -> pd.Index:
        return self._fcm.theta_id

    @property
    def n_theta(self):
        return len(self._fcm.theta_id)


class _XchFluxPrior(Distribution):
    def __init__(
            self,
            model: Union[FluxCoordinateMapper, LabellingModel],
    ):
        # we do not have a support for these priors, since they are checked by the support of
        #   UniFluxPrior anyways
        if isinstance(model, LabellingModel):
            if not model._is_built:
                raise ValueError('First build the model; choose whether to logit exchange fluxes!')
            if model._la.backend != 'torch':
                linalg = LinAlg('torch', seed=model._la._backwargs['seed'])
                model = FluxCoordinateMapper(
                    model,
                    linalg=linalg,
                    free_reaction_id=model.labelling_reactions.list_attr('id'),
                    **model._fcm.fcm_kwargs
                )
            else:
                model = model._fcm

        if model._nx == 0:
            raise ValueError('no boundary fluxes')

        self._la = model._la
        self._xch_id = model.xch_basis_id
        self._logxch = model._logxch
        self._rho_bounds = model._rho_bounds

    def theta_id(self) -> pd.Index:
        return self._xch_id

    def rsample(self, sample_shape: torch.Size = torch.Size()) -> torch.Tensor:
        raise NotImplementedError

    def log_prob(self, value: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class UniXchFluxPrior(_XchFluxPrior):
    def __init__(
            self,
            model: Union[FluxCoordinateMapper, LabellingModel],
    ):
        super().__init__(model)

    def rsample(self, sample_shape: torch.Size = torch.Size()) -> torch.Tensor:
        xch_fluxes = self._la.sample_bounded_distribution(
            shape=sample_shape, lo=self._rho_bounds[:, 0], hi=self._rho_bounds[:, 1]
        )
        if self._logxch:
            xch_fluxes = self._la._logit_xch(xch_fluxes)
        return xch_fluxes

    def log_prob(self, value: torch.Tensor) -> torch.Tensor:
        # we do not check any support here, since that has been done in UniFluxPrior
        return torch.zeros((*value.shape[:-1], 1))


class UniFluxPrior(_NetFluxPrior):
    def __init__(
            self,
            model,
            xch_prior: _XchFluxPrior = None,
            cache_size: int = 20000,
            **kwargs,
    ):
        super(UniFluxPrior, self).__init__(model, cache_size)
        if (self._fcm._nx > 0) and (xch_prior is None):
            xch_prior = UniXchFluxPrior(self._fcm)
        self._xch_prior = xch_prior

    def _fill_caches(self, n=20000, **kwargs):
        n_burn = 500 if self._ic == n else 0
        results = sample_polytope(
            model=self._fcm._sampler, initial_points=self._basis_points, n=n, n_burn=n_burn, new_rounded_points=True,
        )
        theta = results['basis_samples']
        if 'new_rounded_points' in results:
            self._rounded_points = results['new_rounded_points']
        scramble_indices = self._la.randperm(theta.shape[0])
        net_basis_samples = theta[scramble_indices]
        if self._fcm._nx > 0:
            xch_basis_samples = self._xch_prior.sample((n, ))
            theta = self._la.cat([net_basis_samples, xch_basis_samples], dim=-1)
        self._theta_cache = theta

    def log_prob(self, value):
        # log prob for uniform distribution is log(1 / vol(polytope))
        # for non-uniform xch flux distribution, return log(1 / vol(net_polytope)) + log_prob(xch_flux)
        if self._validate_args:
            self._validate_sample(value)
        # place-holder until we can compute polytope volumes

        if (self._fcm._nx > 0) and not isinstance(self._xch_prior, UniXchFluxPrior):
            xch_fluxes = value[..., -self._fcm._nx:]
            return self._xch_prior.log_prob(xch_fluxes)
        return torch.zeros((*value.shape[:-1], 1))


if __name__ == "__main__":
   pass