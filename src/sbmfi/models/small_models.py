import pandas as pd
from collections import OrderedDict
from sbmfi.core.model import LabellingModel, EMU_Model
from sbmfi.inference.priors import UniFluxPrior
from sbmfi.core.observation import ClassicalObservationModel, MVN_BoundaryObservationModel
from sbmfi.core.linalg import LinAlg
from sbmfi.models.build_models import simulator_factory
from sbmfi.settings import MODEL_DIR, SIM_DIR
import sys, os
import cobra
from cobra.io import read_sbml_model
from cobra import Reaction, Metabolite, DictList, Model


def spiro(
        backend='numpy', auto_diff=False, batch_size=1, add_biomass=True, v2_reversible=False,
        ratios=True, build_simulator=False, add_cofactors=True, which_measurements=None, seed=2,
        which_labellings=None, v5_reversible=False, kernel_basis='svd', basis_coordinates='rounded',
        logit_xch_fluxes=False, L_12_omega = 20.0,
):
    if (which_measurements is not None) and not build_simulator:
        raise ValueError

    if v5_reversible:
        v5_atom_map_str = 'F/a + D/bcd  <== C/abcd'
    else:
        v5_atom_map_str = 'F/a + D/bcd  <-- C/abcd'

    reaction_kwargs = {
        'a_in': {
            'lower_bound': 10.0, 'upper_bound': 10.0,
            'atom_map_str': '∅ --> A/ab'
        },
        # 'a_in': {
        #     'lower_bound': -10.0, 'upper_bound': -10.0,
        #     'atom_map_str': 'A/ab --> ∅'
        # },
        'd_out': {
            'upper_bound': 100.0,
            'atom_map_str': 'D/abc --> ∅'
        },
        'f_out': {
            'upper_bound': 100.0,
            'atom_map_str': 'F/a --> ∅'
        },
        'h_out': {
            'upper_bound': 100.0,
            'atom_map_str': 'H/ab --> ∅'
        },
        'v1': {
            'upper_bound': 100.0,
            'atom_map_str': 'A/ab --> B/ab'
        },
        'v2': {
            'lower_bound': 0.0, 'upper_bound': 100.0,
            'rho_min': 0.1, 'rho_max': 0.8,
            'atom_map_str': 'B/ab ==> E/ab'
        },
        'v3': {
            'upper_bound': 100.0,
            'atom_map_str': 'B/ab + E/cd --> C/abcd'
        },
        'v4': {
            'upper_bound': 100.0, # 'lower_bound': -10.0,
            'atom_map_str': 'E/ab --> H/ab'
        },
        # 'v5': {
        #     'upper_bound': 100.0,
        #     'atom_map_str': 'C/abcd --> F/a + D/bcd'
        # },
        'v5': {  # NB this is an always reverse reaction!
            'lower_bound': -100.0, # 'upper_bound': 100.0
            'atom_map_str': v5_atom_map_str,  # <--  ==>
            # 'atom_map_str': 'F/a + D/bcd  <=> C/abcd',  # <--  ==>
        },
        'v6': {
            'upper_bound': 100.0,
            'atom_map_str': 'D/abc --> E/ab + F/c'
        },
        'v7': {
            'upper_bound': 100.0,
            'atom_map_str': 'F/a + F/b --> H/ab'
        },
        'vp': {
            'lower_bound': 0.0, # 'upper_bound': 100.0,
            'pseudo': True,
            'atom_map_str': 'C/abcd + D/efg + H/hi --> L/abgih'
        },
    }
    metabolite_kwargs = {
        'A': {'formula': 'C2H4O5'},
        'B': {'formula': 'C2HPO3'},
        'C': {'formula': 'C4H6N4OS'},
        'D': {'formula': 'C3H2'},
        'E': {'formula': 'C2H4O5'},
        'F': {'formula': 'CH2'},
        'G': {'formula': 'CH2'},  # not used
        'H': {'formula': 'C2H2'},
        'L': {'formula': 'C5KNaSH'},  # pseudo-metabolite
        'L|[1,2]': {'formula': 'C2H2O7'},  # pseudo-metabolite
        'P': {'formula': 'C2H'},
    }

    substrate_df = pd.DataFrame([
        [0.2, 0.0, 0.0, 0.8],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.8, 0.0, 0.2],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.8, 0.2],
    ], columns=['A/00', 'A/01', 'A/10', 'A/11'], index=list('ABCDE'))

    if which_labellings is not None:
        substrate_df = substrate_df.loc[which_labellings]

    if not v2_reversible:
        reaction_kwargs['v2'] = {
            'lower_bound': 0.0, 'upper_bound': 100.0,
            'atom_map_str': 'B/ab --> E/ab'
        }

    annotation_df = pd.DataFrame([
        ('H', 1, 'M-H', 3.0, 0.01, None, 3e3),
        ('H', 0, 'M-H', 2.0, 0.01, None, 3e3),

        ('H', 1, 'M+F', 5.0, 0.03, None, 3e3),
        ('H', 1, 'M+Cl', 88.0, 0.03, None, 2e3),
        ('H', 0, 'M+F', 4.0, 0.03, None, 3e3),  # to indicate that da_df is not yet in any order!
        ('H', 0, 'M+Cl', 89.0, 0.03, None, 2e3),

        ('P', 1, 'M-H', 3.7, 0.02, None, 2e3),  # an annotated metabolite that is not in the model
        ('P', 2, 'M-H', 4.7, 0.02, None, 2e3),
        ('P', 3, 'M-H', 5.7, 0.02, None, 2e3),

        ('C', 0, 'M-H', 1.5, 0.02, None, 7e5),
        ('C', 3, 'M-H', 4.5, 0.02, None, 7e5),
        ('C', 4, 'M-H', 5.5, 0.02, None, 7e5),

        ('D', 2, 'M-H', 12.0, 0.01, None, 1e5),
        ('D', 0, 'M-H', 9.0, 0.01, None, 1e5),
        ('D', 3, 'M-H', 13.0, 0.01, None, 1e5),

        ('L|[1,2]', 0, 'M-H', 14.0, 0.01 * L_12_omega, L_12_omega, 4e4),  # a scaling factor other than 1.0
        ('L|[1,2]', 1, 'M-H', 15.0, 0.01 * L_12_omega, L_12_omega, 4e4),

        ('L', 0, 'M-H', 14.0, 0.01, None, 4e5),
        ('L', 1, 'M-H', 15.0, 0.01, None, 4e5),
        ('L', 2, 'M-H', 16.0, 0.01, None, 4e5),
        ('L', 5, 'M-H', 19.0, 0.01, None, 4e5),
    ], columns=['met_id', 'nC13', 'adduct_name', 'mz', 'sigma', 'omega', 'total_I'])
    formap = {k: v['formula'] for k, v in metabolite_kwargs.items()}
    annotation_df['formula'] = annotation_df['met_id'].map(formap)

    model = simulator_factory(
        id_or_file_or_model='spiro',
        backend=backend,
        auto_diff=auto_diff,
        metabolite_kwargs=metabolite_kwargs,
        reaction_kwargs=reaction_kwargs,
        input_labelling=substrate_df.loc['C'],
        measurements=annotation_df['met_id'].unique(),
        batch_size=batch_size,
        ratios=ratios,
        build_simulator=build_simulator,
        seed=seed,
        kernel_basis=kernel_basis,
        basis_coordinates=basis_coordinates,
        logit_xch_fluxes=logit_xch_fluxes,
    )

    if add_biomass:
        bm = Reaction(id='bm', lower_bound=0.05, upper_bound=1.5)
        bm.add_metabolites(metabolites_to_add={
            model.metabolites.get_by_id('H'): -0.3,
            model.metabolites.get_by_id('B'): -0.6,
            model.metabolites.get_by_id('E'): -0.5,
            model.metabolites.get_by_id('C'): -0.1,
        })
        model.add_reactions(reaction_list=[bm], reaction_kwargs={bm.id: {'atom_map_str': 'biomass --> ∅'}})
        fluxes = {
            'a_in':   10.00,
            # 'a_in_rev':   10.00,
            'd_out':  0.00,
            'f_out':  0.00,
            'h_out':  7.60,
            'v1':     10.00,
            'v2':     1.80,
            'v2_rev': 0.90,
            'v3':     8.20,
            'v4':     0.00,
            'v5':     0.05,
            'v5_rev': 8.10,
            'v6':     8.05,
            'v7':     8.05,
            'bm':     1.50,
        }
        bm = model.reactions.get_by_id('bm')
        model.objective = {bm: 1}
    else:
        fluxes = {
            'a_in': 1.0,
            'd_out': 0.1,
            'f_out': 0.1,
            'h_out': 0.8,
            'v1': 1.0,
            'v2': 0.7,
            'v2_rev': 0.35,
            'v3': 0.7,
            'v4': 0.2,
            'v5': 0.3,
            'v5_rev': 1.0,
            'v6': 0.6,
            'v7': 0.6,
        }
        model.objective = {model.reactions.get_by_id('h_out'): 1}

    if not v2_reversible:
        fluxes['v2'] = fluxes['v2'] - fluxes.pop('v2_rev')

    if not v5_reversible:
        fluxes['v5_rev'] = fluxes['v5_rev'] - fluxes.pop('v5')

    if add_cofactors:
        cof = Metabolite('cof', formula='H2O')
        v3 = model.reactions.get_by_id('v3')
        v3.add_metabolites({cof: 1})
        ex_cof = Reaction('EX_cof', lower_bound=0.0, upper_bound=1000.0)
        ex_cof.add_metabolites({cof: -1})
        model.add_reactions([ex_cof])
        fluxes['EX_cof'] = fluxes['v3']

    fluxes = pd.Series(fluxes, name='v')
    if (batch_size == 1) and build_simulator:
        model.set_fluxes(fluxes=fluxes)

    biomass_id = 'bm' if add_biomass else None
    measured_boundary_fluxes = ['d_out', 'h_out']
    if add_biomass:
        measured_boundary_fluxes.append(biomass_id)

    kwargs = {
        'annotation_df': annotation_df,
        'substrate_df': substrate_df,
        'measured_boundary_fluxes': measured_boundary_fluxes,
        'fluxes': fluxes,
    }

    return model, kwargs


if __name__ == "__main__":
    pass