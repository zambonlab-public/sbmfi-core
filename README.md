# sbmfi-core

This package is a stripped down version of `sbmfi`. 


## Installation notes

This package is pip-installable via `pip install -e .` from the main folder 
(where `setup.py` is located).
Note that the `-e` flag makes this package editable **BY YOU**. 

For now we have the `gurobipy==9.5.1` requirement,
which requires a matching `gurobi` installation on your machine, for which academic licences are available. 
I have this requirement, since `PolyRound` depends on `cobra.utils.solver` solvers.
These solvers are themselves dependent on a package called 
[`optlang`](https://github.com/opencobra/optlang).
I think `gurobi` works best and it's the only solver I've ever used. 
In principle any solver compatible with `optlang` will work, since these solvers also work 
with [`cvxpy`](https://www.cvxpy.org/), which is a far superior optimization library
that I ended up using for all linear programming in `sbmfi`.

We also have `torch` as a requirement, which at almost 1.2GB is quite a hefty package. 
For all the code in `sbmfi.core, sbmfi.lcmsanalysis` and `sbmfi.models` 
it is not a necessary requirement, but for some of the code in `sbmfi.inference` it is.
Specifically, `sbmfi.inference.priors` are defined as `torch.distribution.Distribution`, 
since these objects have an intuitive API and are a requirement for some 
machine-learning stuff I'm working on.

## Using `sbmfi`

Since this package has been under continuuous development, I have not written any documentation yet. 
The simulation part of the software is now static (no major API changes), and the use of it 
is demonstrated in the `sbmfi_demo.ipynb` notebook. 
For this, you need to (`pip`) install Jupyter Lab (which I prefer) or Jupyter Notebook.


# HAVE FUN